Problem ::
^^^^^^^^^^
        Given a rack of 7 letters,determine the highest scoring word that can be formed out of theese 7.the words are scored by adding the score of individual letter as per scrabble rules.
Input ::
^^^^^^^^
        1. The rack of 7 letters.Accepted from the command line. Case not known.

        2. Scoring table
        3. Official word list ::
                * one word per line
                * includes newlines    
                * LENGTH 2 – 15
                * in alphabetical order
                * in upper case.
Output::
^^^^^^^^
        A list  of valid words, sorted by score – descending order.

Process::
^^^^^^^^^
        1. Assume valid input of a rack.
        2. Generate  possible strings containing 2 - 7 letters.
        3. Check whether the strings obtained are in Official word list.
        4. Find the sum of scores of each letter in string.
        5. Compare the sum of each string with the remaining strings.
        6. Sort the score in the descending order and print.
